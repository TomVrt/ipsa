# PSA-project

## Dépendances

- Librairie armadillo (C++)
- numpy (Python)
- swig
- scipy (Python)
- mondoDB
- Doxygen

## Paramètres utilisateurs

Les entrées utilisateurs sont à fournir dans le fichier `src/inputs.json`. Liste des paramètres et leur signification :

- `nMax` : ordre de la solution que l'on souhaite observer
- `xMax` : valeur maximale de l'espace dans lequel on souhaite travailler
- `nbValues` : nombre de valeurs sur un axe (x et y ont le même nombre de valeur et l'espace de travail est un carré)
- `initialWave` : définit avec quelle fonction d'onde de départ on effectue les calculs, pour symboliser des phénomènes physiques différents. Les possibilités sont :
  - `"2D_HO"` pour les solutions de l'oscillateur harmonique à deux dimensions
  - `"YOUNG"` pour représenter une gaussienne avec vitesse initiale traversant des fentes d'Young
  - `"TUNNEL"` pour représenter une gaussienne avec vitesse initiale traversant un mur de potentiel

- `method` : définit le schéma de différence finie utilisé pour le calcul des solutions. Les possibilités sont :
  - `FTCS` pour la méthode explicite Forward Time Center Space
  - `BTCS` pour la méthode implicite Backward Time Center Space
  - `CTCS` pour la méthode de Crank-Nicolson Center Time Center Space
