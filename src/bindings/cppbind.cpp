#include "cppbind.h"

/**
 * \file cppbind.cpp
 * \author Anteunis Charles
 * \brief Code des différentes méthodes de différences finies pour le calcul des itérations. Les itérations sont calculées à l'aide de la fonction suivante, qu'on appelle g :
 * TODO: écrire en Latex la fonction g des slides pour les itérations
 */

/**
 * \brief Méthode de différence finie explicite FTCS
 * \param mat matrice de la fonction g à l'itération n
 * \return la matrice de la fonction g à l'itération n+1
 */
arma::cx_mat Cppbind::ftcs(arma::cx_mat mat) {
    //TODO: code de la méthode FTCS
    arma::cx_mat res(arma::size(mat));
    arma::mat mat_real = arma::real(mat);
    arma::mat mat_imag = arma::imag(mat);
    int n = mat.n_rows;
    int nc = mat.n_cols;
    const double hbar = arma::datum::h_bar;
    arma::cx_double mat_ip1 = 0, mat_im1 = 0, mat_jp1 = 0, mat_jm1 = 0;
    for (int i = 0 ; i<n ; i++) {
        for (int j = 0; j<nc ; j++)
        {
            //FIXME: pas de séparation réelle et imaginaire, multiplier les complexes directement
            if (i == 0) {
                mat_im1 = 0;
                mat_ip1 = mat(i+1,j);
            }
            else if (i == n-1) {
                mat_im1 = mat(i-1,j);
                mat_ip1 = 0;
            }
            else {
                mat_im1 = mat(i-1,j);
                mat_ip1 = mat(i+1,j);
            }
            if (j == 0) {
                mat_jm1 = 0;
                mat_jp1 = mat(i,j+1);
            }
            else if (j == nc-1) {
                mat_jm1 = mat(i,j-1);
                mat_jp1 = 0;
            }
            else {
                mat_jp1 = mat(i,j+1);
                mat_jm1 = mat(i,j-1);
            }
            res(i,j) = 
                arma::cx_double(0,-dt/hbar) * (
                    -hbar*hbar/(2*m) * (mat_ip1 + mat_im1)
                    -hbar*hbar/(2*m) * (mat_jp1 + mat_jm1)
                    +mat(i,j) * (2*hbar*hbar/m + myPot(i,j) + arma::cx_double(0,hbar/dt))
                );
        }
        
    }
    return res;
}

/**
 * \brief Méthode de différence finie implicite BTCS
 * \param mat matrice de la fonction g à l'itération n
 * \return la matrice de la fonction g à l'itération n+1
 */
arma::cx_mat Cppbind::btcs(arma::cx_mat mat) {
    //TODO: code de la méthode BTCS
    return arma::cx_mat(3, 3, arma::fill::ones);
}

/**
 * \brief Méthode de différence finie explicite CTCS
 * \param mat matrice de la fonction g à l'itération n
 * \return la matrice de la fonction g à l'itération n+1
 */
arma::cx_mat Cppbind::ctcs(arma::cx_mat mat) {
    //TODO: code de la méthode CTCS
    return arma::cx_mat(3, 3, arma::fill::ones);
}

/**
 * \brief Calcul de la norme de la différence entre deux itérations de g pour stopper l'itération selon un epsilon particulier
 * \param mat_before matrice de g à l'itération n
 * \param mat_after  matrice de g à l'itération n+1
 * \return la norme de g_n+1 - g_n
 * TODO: écrire la norme en Latex
 */
double Cppbind::norme(arma::cx_mat a, arma::cx_mat b) {
    //TODO: code de la norme de la différence (en deux dimensions ?)
    return 0.0;
}

/**
 * TODO: Documentation à faire
 */
Cppbind::Cppbind(double _dt, double _m, arma::cx_mat _mat, arma::mat _pot) : dt(_dt), m(_m), myMat(_mat), myPot(_pot)
{
}
