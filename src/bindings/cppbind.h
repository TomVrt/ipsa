#ifndef CPPBIND_H
#define CPPBIND_H

#include <armadillo>
#include <complex>

/**
 * \file cppbind.h
 * \author Anteunis Charles
 * \brief Interface des différentes fonctions de différences finies pour le calcul des itérations
 */

class Cppbind
{
public:
    Cppbind(double, double, arma::cx_mat _mat = arma::cx_mat(), arma::mat _pot = arma::mat());

    arma::cx_mat ftcs(arma::cx_mat);

    arma::cx_mat btcs(arma::cx_mat);

    arma::cx_mat ctcs(arma::cx_mat);

    arma::cx_mat myMat;

    arma::mat myPot;

    double dt;

    double m;

    double norme(arma::cx_mat, arma::cx_mat);
};

#endif
