"""@package bindstest
Module python des tests unitaires des bindings C++ aramdillo

Contient les tests d'appels aux fonctions implémentées en C++ pour s'assurer du bon fonctionnement des bindings.
Ne teste pas la validité des résultats retournés par les fonctions C++.
"""
import unittest
import numpy as np
import cppbind

arrR = np.array([[0.1, 0.2], [0.3, 0.4]], dtype = np.float64, order = 'F')
arrI = np.array([[0.1+0.j, 0.2+0.j], [0.3+0.j, 0.4+0.j]], dtype = complex, order = 'F')

class TestCppbindMethods(unittest.TestCase):

   
    def test_constructor(self):
        objCpp = cppbind.Cppbind(1, 1, arrI, arrR)
        self.assertEqual(arrI.all() ,objCpp.myMat.all())

    def test_ftcs(self):
        self.assertEqual(0, 0)

    def test_btcs(self):
        self.assertEqual(0, 0)

    def test_ctcs(self):
        self.assertEqual(0, 0)



if __name__ == '__main__':
    unittest.main()
