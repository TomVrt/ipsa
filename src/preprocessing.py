"""@package preprocessing
Module python du Field Generator

Contient les différentes fonctions pour générer les fonctions d'ondes et potentiels initiaux.
Les différentes générations possibles sont : les solutions 2D-HO, les fentes d'Young et l'effet tunnel.
"""
import numpy as np
import math
from scipy import constants
import json

def dho() :
    """Génération des matrices initiales pour les solutions 2D-HO

    Génère à partir des entrées de inputs.json la fonction d'onde initiale d'une solution 2D-HO ainsi que le potentiel initial.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """

    with open('inputs.json') as json_file :
        inputs = json.load(json_file)
        nbValues = inputs['nbValues'] # le nombre de valeurs en lesquelles on évalue la solution
        nMax = inputs['nMax'] # le rang de la solution
        iMax = inputs['xMax'] # jusqu'où on évalue sur l'axe X les solutions

    m = 10**-32
    omega = 10**-2

    coefs = []
    psi = []
    res = []
    potentiel = []
    # on initialise le vecteurs des valeurs sur x
    X = np.linspace(0,iMax,nbValues,endpoint=False)

    print("Génération de la fonction d'onde et du potentiel initiaux 2D-HO...")

    for i in X :
        coefs.append( 1/math.sqrt(2**nMax * math.factorial(nMax)) * math.pow(m*omega/math.pi/constants.hbar,1/4) * math.exp(-m*omega*i**2/2/constants.hbar) )

    for i in range(nbValues) :
        psi.append(np.polynomial.hermite.Hermite(coef=[0] * (nMax-1) + [coefs[i]])(math.sqrt(m*omega/constants.hbar) * X[i]) )

    # psi contient maintent toutes les valeurs psi_nMax(x) pour x entre 0 et iMax
    # on construit la matrices en multipliant psi(x) et psi(y) comme dit dans les slides
    for i in range(nbValues) :
        res.append([])
        potentiel.append([])
        for j in range(nbValues) :
            res[i].append(complex(psi[i]*psi[j], 0))
            potentiel[i].append(1/2 * m*omega**2*(i**2+j**2))

    json_file.close()
    return(np.array(res, dtype = complex, order = 'F'),np.array(potentiel, dtype = np.float64, order = 'F'))

def young() :
    """Génération des matrices initiales pour les fentes d'Young

    Génère à partir des entrées de inputs.json la fonction d'onde initiale pour l'expérience des fentes d'Young ainsi que le potentiel initial.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """
    #TODO: génération des matrices
    #TODO: entrées utilisateurs pour les paramètres de Young
    print("Matrices initiales pour les fentes d'Young à implémenter")
    potentiel = []
    res = []
    return(np.array(res, dtype = complex, order = 'F'), np.array(potentiel, dtype = np.float64, order = 'F'))

def tunnel() :
    """Génération des matrices initiales pour l'effet tunnel

    Génère à partir des entrées de inputs.json la fonction d'onde initiale pour l'expérience de l'effet tunnel ainsi que le potentiel initial.
    Retourne le couple de matrices (fonction d'onde, potentiel).
    """
    #TODO: génération des matrices
    #TODO: entrées utilisateurs pour les paramètres de tunnel
    print("Matrices initiales pour l'effet tunnel à implémenter")
    potentiel = []
    res = []
    return(np.array(res, dtype = complex, order = 'F'), np.array(potentiel, dtype = np.float64, order = 'F'))

#FIXME: juste pour débug, à enlever à la fin
# res,pot = dho()
# print(res)
