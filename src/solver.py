"""@package solver
Module pour le calcul des solutions de l'équation de Schrödinger non-relativiste dépendante du temps, en deux dimensions

Le calcul de la propagation en fonction du temps peut s'effectuer via différents schéma de différences finies.
Les schémas disponibles sont : FTCS, BTCS et CTCS. Les calculs font appel à du code C++ et à la librairie armadillo.
Les résultats sont stockés dans une base de données MongoDB.
"""
import json
import preprocessing
import numpy as np
import matplotlib.pyplot as plt
import cppbind

psi, V = np.array([]), np.array([])

def ftcs(myClass, mat) :
    """Schéma FTCS : Forward Time, Center Space
    """
    print("ftcs à implémenter")
    return myClass.ftcs(mat)

def btcs(myClass, mat) :
    """Schéma BTCS : Backward Time, Center Space
    """
    print("btcs à implémenter")

def ctcs(myClass, mat) :
    """Schéma CTCS : Center Time, Center Space
    """
    print("ctcs à implémenter")

def iter(init_psi, init_V, f, epsilon=0.001):
    """Fonction principale pour lancer le calcul de chaque itération et stocker le résultat
    """
    print("Itérations de la propagation dynamique...")
    #TODO: code de la boucle principale 
    myClass = cppbind.Cppbind(1, 10**-32, init_psi, init_V)
    norme = 4
    old = init_psi
    while norme > epsilon :
        res = f(myClass, old)
        norme = myClass.norme(res, old)
        old = res

    plt.matshow(np.absolute(init_psi))
    #plt.matshow(np.real(res))
    #plt.matshow(np.imag(res))
    plt.matshow(np.absolute(res))
    res2 = f(myClass, res)
    plt.matshow(np.absolute(res2))
    plt.colorbar()
    plt.show()
    print(np.linalg.norm(res2-res))

with open('inputs.json') as json_file:
    inputs = json.load(json_file)
    method = inputs['method']
    initial = inputs['initialWave']

if initial == "2D-HO":
    psi, V = preprocessing.dho()
elif initial == "YOUNG":
    psi, V = preprocessing.young()
elif initial == "TUNNEL":
    psi, V = preprocessing.tunnel()
else :
    print("Erreur de fonction initale dans le fichier inputs.json .")
    print("Veuillez vérifier que l'initialWave soit 2D-HO, YOUNG ou TUNNEL.")
    exit(1)
    
if method == "FTCS":
    iter(psi, V, ftcs)
elif method == "BTCS":
    iter(psi, V, btcs)
elif method == "CTCS":
    iter(psi, V, ctcs)
else:
    print("Erreur de méthode dans le fichier inputs.json.")
    print("Veuillez vérifier que la méthode soit FTCS, BTCS ou CTCS.")
    exit(1)